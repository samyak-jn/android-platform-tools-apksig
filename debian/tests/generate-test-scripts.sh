#!/bin/sh -e
#

set -x
test -d ../../debian/tests || (echo "run in debian/tests/ subdir of package"; exit 1)
basedir=$(cd $(dirname $0)/../..; pwd)

cat <<EOF > $basedir/debian/tests/ApkVerifierTest.sh
#!/bin/sh -e
set -x

if ! apksigner; then
    echo "WARNING using local script hack, binfmt probably not setup"
    test -e bin || mkdir bin
    printf '#!/bin/sh -e\njava -jar /usr/bin/apksigner \$@\n' > bin/apksigner
    chmod 0755 bin/apksigner
    export PATH=\`pwd\`/bin:\$PATH
fi

EOF

cd $basedir

for f in $(ls -1 `grep -A1 'assertVerifiedForEach(' src/test/java/com/android/apksig/ApkVerifierTest.java \
  | grep -Eo '"v[12]-.*\.apk"' \
  | sed -e 's,%s,[0-9p][0-9]*,' -e 's,^",src/test/resources/com/android/apksig/,' -e 's,",,g'`); do
    echo apksigner verify $f  >> $basedir/debian/tests/ApkVerifierTest.sh
done

for f in `grep -A1 'assertVerificationFailure(' src/test/java/com/android/apksig/ApkVerifierTest.java \
  | grep -Eo ' "v[12]-.*\.apk"' \
  | sed -e 's,^ ",src/test/resources/com/android/apksig/,' -e 's,",,g'`; do
    echo '! apksigner verify' $f  >> $basedir/debian/tests/ApkVerifierTest.sh
done

echo "echo SUCCESS" >> $basedir/debian/tests/ApkVerifierTest.sh
