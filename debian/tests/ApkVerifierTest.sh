#!/bin/sh -e
set -x

if ! apksigner; then
    echo "WARNING using local script hack, binfmt probably not setup"
    test -e bin || mkdir bin
    printf '#!/bin/sh -e\njava -jar /usr/bin/apksigner $@\n' > bin/apksigner
    chmod 0755 bin/apksigner
    export PATH=`pwd`/bin:$PATH
fi

apksigner verify src/test/resources/com/android/apksig/v1-only-with-dsa-sha1-1.2.840.10040.4.1-1024.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-dsa-sha1-1.2.840.10040.4.1-1024.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-dsa-sha1-1.2.840.10040.4.1-2048.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-dsa-sha1-1.2.840.10040.4.1-2048.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-dsa-sha1-1.2.840.10040.4.1-3072.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-dsa-sha1-1.2.840.10040.4.1-3072.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-dsa-sha1-1.2.840.10040.4.3-1024.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-dsa-sha1-1.2.840.10040.4.3-1024.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-dsa-sha1-1.2.840.10040.4.3-2048.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-dsa-sha1-1.2.840.10040.4.3-2048.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-dsa-sha1-1.2.840.10040.4.3-3072.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-dsa-sha1-1.2.840.10040.4.3-3072.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-dsa-sha224-1.2.840.10040.4.1-1024.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-dsa-sha224-1.2.840.10040.4.1-1024.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-dsa-sha224-1.2.840.10040.4.1-2048.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-dsa-sha224-1.2.840.10040.4.1-2048.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-dsa-sha224-1.2.840.10040.4.1-3072.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-dsa-sha224-1.2.840.10040.4.1-3072.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-dsa-sha224-2.16.840.1.101.3.4.3.1-1024.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-dsa-sha224-2.16.840.1.101.3.4.3.1-1024.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-dsa-sha224-2.16.840.1.101.3.4.3.1-2048.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-dsa-sha224-2.16.840.1.101.3.4.3.1-2048.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-dsa-sha224-2.16.840.1.101.3.4.3.1-3072.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-dsa-sha224-2.16.840.1.101.3.4.3.1-3072.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-dsa-sha256-1.2.840.10040.4.1-1024.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-dsa-sha256-1.2.840.10040.4.1-2048.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-dsa-sha256-1.2.840.10040.4.1-3072.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-dsa-sha256-2.16.840.1.101.3.4.3.2-1024.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-dsa-sha256-2.16.840.1.101.3.4.3.2-2048.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-dsa-sha256-2.16.840.1.101.3.4.3.2-3072.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-ecdsa-sha1-1.2.840.10045.2.1-p256.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-ecdsa-sha1-1.2.840.10045.2.1-p384.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-ecdsa-sha1-1.2.840.10045.2.1-p521.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-ecdsa-sha1-1.2.840.10045.4.1-p256.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-ecdsa-sha1-1.2.840.10045.4.1-p384.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-ecdsa-sha1-1.2.840.10045.4.1-p521.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-ecdsa-sha224-1.2.840.10045.2.1-p256.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-ecdsa-sha224-1.2.840.10045.2.1-p384.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-ecdsa-sha224-1.2.840.10045.2.1-p521.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-ecdsa-sha224-1.2.840.10045.4.3.1-p256.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-ecdsa-sha224-1.2.840.10045.4.3.1-p384.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-ecdsa-sha224-1.2.840.10045.4.3.1-p521.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-ecdsa-sha256-1.2.840.10045.2.1-p256.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-ecdsa-sha256-1.2.840.10045.2.1-p384.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-ecdsa-sha256-1.2.840.10045.2.1-p521.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-ecdsa-sha256-1.2.840.10045.4.3.2-p256.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-ecdsa-sha256-1.2.840.10045.4.3.2-p384.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-ecdsa-sha256-1.2.840.10045.4.3.2-p521.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-ecdsa-sha384-1.2.840.10045.2.1-p256.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-ecdsa-sha384-1.2.840.10045.2.1-p384.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-ecdsa-sha384-1.2.840.10045.2.1-p521.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-ecdsa-sha384-1.2.840.10045.4.3.3-p256.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-ecdsa-sha384-1.2.840.10045.4.3.3-p384.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-ecdsa-sha384-1.2.840.10045.4.3.3-p521.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-ecdsa-sha512-1.2.840.10045.2.1-p256.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-ecdsa-sha512-1.2.840.10045.2.1-p384.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-ecdsa-sha512-1.2.840.10045.2.1-p521.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-ecdsa-sha512-1.2.840.10045.4.3.4-p256.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-ecdsa-sha512-1.2.840.10045.4.3.4-p384.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-ecdsa-sha512-1.2.840.10045.4.3.4-p521.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-md5-1.2.840.113549.1.1.1-1024.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-md5-1.2.840.113549.1.1.1-16384.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-md5-1.2.840.113549.1.1.1-2048.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-md5-1.2.840.113549.1.1.1-3072.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-md5-1.2.840.113549.1.1.1-4096.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-md5-1.2.840.113549.1.1.1-8192.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-md5-1.2.840.113549.1.1.4-1024.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-md5-1.2.840.113549.1.1.4-16384.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-md5-1.2.840.113549.1.1.4-2048.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-md5-1.2.840.113549.1.1.4-3072.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-md5-1.2.840.113549.1.1.4-4096.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-md5-1.2.840.113549.1.1.4-8192.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha1-1.2.840.113549.1.1.1-1024.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha1-1.2.840.113549.1.1.1-16384.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha1-1.2.840.113549.1.1.1-2048.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha1-1.2.840.113549.1.1.1-3072.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha1-1.2.840.113549.1.1.1-4096.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha1-1.2.840.113549.1.1.1-8192.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha1-1.2.840.113549.1.1.5-1024.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha1-1.2.840.113549.1.1.5-16384.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha1-1.2.840.113549.1.1.5-2048.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha1-1.2.840.113549.1.1.5-3072.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha1-1.2.840.113549.1.1.5-4096.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha1-1.2.840.113549.1.1.5-8192.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha224-1.2.840.113549.1.1.1-1024.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha224-1.2.840.113549.1.1.1-16384.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha224-1.2.840.113549.1.1.1-2048.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha224-1.2.840.113549.1.1.1-3072.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha224-1.2.840.113549.1.1.1-4096.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha224-1.2.840.113549.1.1.14-1024.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha224-1.2.840.113549.1.1.14-16384.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha224-1.2.840.113549.1.1.14-2048.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha224-1.2.840.113549.1.1.14-3072.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha224-1.2.840.113549.1.1.14-4096.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha224-1.2.840.113549.1.1.14-8192.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha224-1.2.840.113549.1.1.1-8192.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha256-1.2.840.113549.1.1.1-1024.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha256-1.2.840.113549.1.1.11-1024.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha256-1.2.840.113549.1.1.11-16384.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha256-1.2.840.113549.1.1.11-2048.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha256-1.2.840.113549.1.1.11-3072.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha256-1.2.840.113549.1.1.11-4096.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha256-1.2.840.113549.1.1.1-16384.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha256-1.2.840.113549.1.1.11-8192.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha256-1.2.840.113549.1.1.1-2048.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha256-1.2.840.113549.1.1.1-3072.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha256-1.2.840.113549.1.1.1-4096.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha256-1.2.840.113549.1.1.1-8192.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha384-1.2.840.113549.1.1.1-1024.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha384-1.2.840.113549.1.1.1-16384.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha384-1.2.840.113549.1.1.1-2048.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha384-1.2.840.113549.1.1.12-1024.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha384-1.2.840.113549.1.1.12-16384.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha384-1.2.840.113549.1.1.12-2048.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha384-1.2.840.113549.1.1.12-3072.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha384-1.2.840.113549.1.1.12-4096.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha384-1.2.840.113549.1.1.12-8192.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha384-1.2.840.113549.1.1.1-3072.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha384-1.2.840.113549.1.1.1-4096.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha384-1.2.840.113549.1.1.1-8192.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha512-1.2.840.113549.1.1.1-1024.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha512-1.2.840.113549.1.1.1-16384.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha512-1.2.840.113549.1.1.1-2048.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha512-1.2.840.113549.1.1.1-3072.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha512-1.2.840.113549.1.1.13-1024.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha512-1.2.840.113549.1.1.13-16384.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha512-1.2.840.113549.1.1.13-2048.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha512-1.2.840.113549.1.1.13-3072.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha512-1.2.840.113549.1.1.13-4096.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha512-1.2.840.113549.1.1.13-8192.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha512-1.2.840.113549.1.1.1-4096.apk
apksigner verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha512-1.2.840.113549.1.1.1-8192.apk
! apksigner verify src/test/resources/com/android/apksig/v2-stripped.apk
! apksigner verify src/test/resources/com/android/apksig/v2-stripped-with-ignorable-signing-schemes.apk
! apksigner verify src/test/resources/com/android/apksig/v2-only-with-dsa-sha256-1024-sig-does-not-verify.apk
! apksigner verify src/test/resources/com/android/apksig/v2-only-with-rsa-pkcs1-sha256-2048-sig-does-not-verify.apk
! apksigner verify src/test/resources/com/android/apksig/v2-only-with-ecdsa-sha256-p256-sig-does-not-verify.apk
! apksigner verify src/test/resources/com/android/apksig/v2-only-with-rsa-pss-sha256-2048-sig-does-not-verify.apk
! apksigner verify src/test/resources/com/android/apksig/v2-only-with-rsa-pkcs1-sha512-4096-digest-mismatch.apk
! apksigner verify src/test/resources/com/android/apksig/v2-only-with-ecdsa-sha256-p256-digest-mismatch.apk
! apksigner verify src/test/resources/com/android/apksig/v2-only-wrong-apk-sig-block-magic.apk
! apksigner verify src/test/resources/com/android/apksig/v2-only-garbage-between-cd-and-eocd.apk
! apksigner verify src/test/resources/com/android/apksig/v2-only-apk-sig-block-size-mismatch.apk
! apksigner verify src/test/resources/com/android/apksig/v2-only-signatures-and-digests-block-mismatch.apk
! apksigner verify src/test/resources/com/android/apksig/v2-only-cert-and-public-key-mismatch.apk
! apksigner verify src/test/resources/com/android/apksig/v2-only-no-certs-in-sig.apk
! apksigner verify src/test/resources/com/android/apksig/v2-only-two-signers-second-signer-no-sig.apk
! apksigner verify src/test/resources/com/android/apksig/v2-only-two-signers-second-signer-no-supported-sig.apk
echo SUCCESS
